# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR the phpMyAdmin project
# This file is distributed under the same license as the phpMyAdmin documentation package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: phpMyAdmin documentation VERSION\n"
"Report-Msgid-Bugs-To: phpmyadmin-devel@lists.sourceforge.net\n"
"POT-Creation-Date: 2012-02-04 14:15+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#: addendum/comment.html:2
msgid "<!--"
msgstr ""

#. type: Plain text
#: addendum/comment.html:4
msgid "This file is generated using po4a, do not edit!"
msgstr ""

#. type: Plain text
#: addendum/comment.html:6
msgid "You can edit po files to change the translation."
msgstr ""

#. type: Plain text
#: addendum/comment.html:8
msgid "Or you can edit them online at https://l10n.cihar.com/projects/pmadoc/."
msgstr ""

#. type: Plain text
#: addendum/comment.html:9
msgid "-->"
msgstr ""

#. type: Title =
#: orig-docs/INSTALL:2
#, no-wrap
msgid "phpMyAdmin - Installation"
msgstr ""

#. type: Plain text
#: orig-docs/INSTALL:5
msgid "Please have a look to the Documentation.txt or Documentation.html files."
msgstr ""

#. type: Title =
#: orig-docs/TODO:2
#, no-wrap
msgid "phpMyAdmin - Todo"
msgstr ""

#. type: Plain text
#: orig-docs/TODO:5
msgid "We are currently using the Sourceforge Tracker as Todo list:"
msgstr ""

#. type: Plain text
#: orig-docs/TODO:7
#, no-wrap
msgid "  http://sourceforge.net/tracker/?atid=377411&group_id=23067&func=browse\n"
msgstr ""

#. type: Plain text
#: orig-docs/TODO:8
msgid "-- swix/20010704"
msgstr ""

#. type: Title =
#: orig-docs/README:2
#, no-wrap
msgid "phpMyAdmin - Readme"
msgstr ""

#. type: Plain text
#: orig-docs/README:5
msgid "Version @@VER@@"
msgstr ""

#. type: Plain text
#: orig-docs/README:7
msgid "A set of PHP-scripts to manage MySQL over the web."
msgstr ""

#. type: Plain text
#: orig-docs/README:9
msgid "http://www.phpmyadmin.net/"
msgstr ""

#. type: Title -
#: orig-docs/README:11
#, no-wrap
msgid "Copyright"
msgstr ""

#. type: Plain text
#: orig-docs/README:15
#, no-wrap
msgid ""
"Copyright (C) 1998-2000\n"
"    Tobias Ratschiller <tobias_at_ratschiller.com>\n"
msgstr ""

#. type: Plain text
#: orig-docs/README:26
#, no-wrap
msgid ""
"Copyright (C) 2001-2012\n"
"    Marc Delisle <marc_at_infomarc.info>\n"
"    Olivier Müller <om_at_omnis.ch>\n"
"    Robin Johnson <robbat2_at_users.sourceforge.net>\n"
"    Alexander M. Turek <me_at_derrabus.de>\n"
"    Michal Čihař <michal_at_cihar.com>\n"
"    Garvin Hicking <me_at_supergarv.de>\n"
"    Michael Keck <mkkeck_at_users.sourceforge.net>\n"
"    Sebastian Mendel <cybot_tm_at_users.sourceforge.net>\n"
"    [check Documentation.txt/.html file for more details]\n"
msgstr ""

#. type: Title -
#: orig-docs/README:28
#, no-wrap
msgid "License"
msgstr ""

#. type: Plain text
#: orig-docs/README:33
msgid ""
"This program is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License version 2, as published by "
"the Free Software Foundation."
msgstr ""

#. type: Plain text
#: orig-docs/README:38
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""

#. type: Plain text
#: orig-docs/README:42
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin St, Fifth Floor, Boston, MA 02110-1301 USA"
msgstr ""

#. type: Title -
#: orig-docs/README:44
#, no-wrap
msgid "Requirements"
msgstr ""

#. type: Plain text
#: orig-docs/README:47
msgid "PHP 5.2 or later"
msgstr ""

#. type: Plain text
#: orig-docs/README:48
msgid "MySQL 5.0 or later"
msgstr ""

#. type: Plain text
#: orig-docs/README:49
msgid "a web-browser (doh!)"
msgstr ""

#. type: Title -
#: orig-docs/README:51
#, no-wrap
msgid "Summary"
msgstr ""

#. type: Plain text
#: orig-docs/README:55
msgid ""
"phpMyAdmin is intended to handle the administration of MySQL over the web.  "
"For a summary of features, please see the Documentation.txt/.html file."
msgstr ""

#. type: Title -
#: orig-docs/README:57
#, no-wrap
msgid "Download"
msgstr ""

#. type: Plain text
#: orig-docs/README:60
msgid "You can get the newest version at http://www.phpmyadmin.net/."
msgstr ""

#. type: Title -
#: orig-docs/README:62
#, no-wrap
msgid "More Information"
msgstr ""

#. type: Plain text
#: orig-docs/README:65
msgid "Please see the Documentation.txt/.html file."
msgstr ""

#. type: Title -
#: orig-docs/README:67
#, no-wrap
msgid "Support"
msgstr ""

#. type: Plain text
#: orig-docs/README:70
msgid "See reference about support forums under http://www.phpmyadmin.net/"
msgstr ""

#. type: Title -
#: orig-docs/README:73
#, no-wrap
msgid "Enjoy!"
msgstr ""

#. type: Plain text
#: orig-docs/README:76
msgid "The phpMyAdmin Devel team"
msgstr ""

#. type: Plain text
#: orig-docs/README:79
msgid "PS:"
msgstr ""

#. type: Plain text
#: orig-docs/README:82
msgid ""
"Please, don't send us emails with question like \"How do I compile PHP with "
"MySQL-support\". We just don't have the time to be your free help desk."
msgstr ""

#. type: Plain text
#: orig-docs/README:85
msgid ""
"Please send your questions to the appropriate mailing lists / forums.  "
"Before contacting us, please read the Documentation.html (esp. the FAQ "
"part)."
msgstr ""
